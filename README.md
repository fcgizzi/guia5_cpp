# Árbol con id de pdb

Se solicita un programa que cree un árbol balanceado en el que se muestren datos de un archivo txt el cual contiene cierta candidad de id de proteinas de pdb, así como también, se le debe permitir al usuario ingresar datos, eliminar y modificar.

# Compilación
- Se escribe `make` en la terminal, si no se tiene, se instala con anterioridad ingresando `sudo apt install make` en la terminal

- Si no se tiene make, se usa `g++ main.cpp Arbol.cpp Grafo.cpp -o main` para compilar

# Ejecución

Para ejecutar el programa el programa se ingresa `./main` en la terminal

# Programa

El programa al inciarse entrega tres opciones de archivos txt, cada uno con una cantidad estimada de id
 - Un archivo con 10, otro con 100 y el último con 400 datos dentro

1. Ingreso de elemento: Esta opción hará que el usuario ingrese un valor numérico, el cual, posteriormente se añadirá en el árbol. 
2. Buscar elemento: Esta opción permite al usuario buscar el id desseado dentro del árbol.
3. Eliminar elemento: Parecido a lo anterior, se solicita el número que se desea eliminar, este es buscado en el árbol y se elimina.
4. Modificar elemento: Esta opción usa las anteriores mencionadas, primeros pide elemento que se desea eliminar, se busca, se borra y posterior a esto se pide el dato por el cual se desea remplazar el dato anterior.
5. Generar grafo: Esta opción hace que se generen dos archivos, un .txt y un .png, los cuales harán que se visualice una imagen gráfica del arbol que contiene los pdbs y los datos que talvez el usuario ingresó.
6. Salir: Opcipin para salir del programa.

# Requisitos

- Sistema operativo Linux
- Make instalado
- Paquete graphviz. Se instala de la sigueinte manera: `sudo apt-get install graphviz` 

# Construido con
- Linux
- Geany
- Lenguaje c++

# Autor
- Franco Cifuentes Gizzi