/*
 * Compilación: $ make
 * Ejecución: $ ./main
 */

// librerias
#include <unistd.h>
#include <fstream>
#include <string>
#include <iostream>
using namespace std;

// clases
#include "main.h"
#include "Arbol.h"
#include "Grafo.h"

#define TRUE 1
#define FALSE 0

// función menú
void menu(){
	// variables
	int op, pdbs;
	// elemento y elemento2 para modificar
	string txt, elemento, elemento2;
	// booleanos que indican eliminación, inserción y prueba
	bool b_eliminar, b_insertar, prueba;
	int inicio;

	// se crea el arbol
	Arbol *arbol = new Arbol();
	// se hace escoger al usario entre 3 archivos, 
	// entre más id más demora tomará en funcionar el programa
	cout << "\t--------------------------------------" << endl;
	cout << "\t| Seleccine el archivo que utilizará |" << endl;
	cout << "\t| 1 >> pdbs de 10 id                 |" << endl;
	cout << "\t| 2 >> pdbs de 100 id                |" << endl;
	cout << "\t| 3 >> pdbs de 400 id                |" << endl;
	cout << "\t--------------------------------------" << endl;
	cout << "\t >> ";
	cin >> pdbs;
	// variable txt tomará valor del archivo elegido
	if(pdbs == 1){
		txt = "pdbs_10.txt";
	}
	else if(pdbs == 2){
		txt = "pdbs_100.txt";
	}
	else if(pdbs == 3){
		txt = "pdbs_400.txt";
	}
	
	// se abre el archivo elegido usando vairable txt
	ifstream archivo(txt);
	string linea;
	
	// limpia la pantalla
	system("clear");
	// se crea nodo raiz
	Nodo *raiz = NULL;

	// se llena el arbol
	while(getline(archivo, linea)){
		inicio = FALSE;
		// se insertan los id en el arbol
		arbol->InsercionBalanceado(raiz, &inicio, linea);
	}

	// función do que se repite hasta ingresar lo indicado
	do{
		cout << "\n\t------------Menú-------------" << endl;
		cout << "\t| Eliga una opción:         |" << endl;
		cout << "\t| 1 >> Insertar             |" << endl;
		cout << "\t| 2 >> Buscar               |" << endl;
		cout << "\t| 3 >> Eliminar             |" << endl;
		cout << "\t| 4 >> Modificar            |" << endl;
		cout << "\t| 5 >> Mostrar grafo        |" << endl;
		cout << "\t| 6 >> Salir                |" << endl;
		cout << "\t-----------------------------" << endl;
		cout << "\t >> ";
		cin >> op;
		// limpia la pantalla
		system("clear");
				
		// switch con las opciones
		switch(op){
			
			// caso op 1
			case 1:
				cout << "\t>> Qué desea ingresar: ";
				cin >> elemento;
				inicio = FALSE;
				// se llama a la función que inserta elemento al arbol
				arbol->InsercionBalanceado(raiz, &inicio, elemento);
			break;
			
			// caso op 2
			case 2:
				cout << "\t>> Qué desea buscar: ";
				cin >> elemento;
				// se llama a la función busca elemento
				arbol->Busqueda(raiz, elemento, prueba);
			break;
			
			//caso op 3
			case 3:
				cout << "\t>> Qué desea eliminar: ";
				cin >> elemento;
				inicio = FALSE;
				// se llama a la función que elimina elemento
				arbol->EliminacionBalanceado(&raiz, &inicio, elemento);
			break;
			
			//caso op 4
			case 4:
				cout << "\t>> Qué desea modificar: ";
				cin >> elemento;
				// primero el elemento debe existir
				arbol->Busqueda(raiz, elemento, b_eliminar);
				// si está en el arbol
				if(b_eliminar){
					// se ingresa en reemplazo
					cout << "\t>> Cambiar por: ";
					cin >> elemento2;
					// busca para ver si se puede insertar
					arbol->Busqueda(raiz, elemento2, b_insertar);
					// si no existe en el arbol
					if(b_insertar == false){
						// llama a la función de eliminación e inserción
						arbol->EliminacionBalanceado(&raiz, &inicio, elemento);
						arbol->InsercionBalanceado(raiz, &inicio, elemento2);
						cout << " \t>> Se ha cambiado el valor: " << elemento << " por: " << elemento2 << endl;
					}
					else{
						cout << "\t>> No existe" << endl;
					}
				}
				else{
					cout << " \t>> No existe" << endl;
				}
			break;
			
			// caso op 5
			case 5:
				// se llama a función que crea grafo
				arbol->crear_grafo(raiz);
			break;

			// caso op 6
			case 6:
				system("clear");
				cout << "\t --ADIOS--" << endl;
				exit(1);
			break;
		}

	} // fin del do
	// mientras op sea menor que 7 y mayor que 0 
	while (op < 7 && op > 0);
}

// función principal
int main(int argc, char **argv) {
	// función menú
	menu();

  return 0;
}
