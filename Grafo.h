// librerias
#include <string>
#include <fstream>
#include <unistd.h>
#include <iostream>
using namespace std;

// constructores de clase
#ifndef GRAFO_H
#define GRAFO_H

class Grafo {
    private:

    public:
        //constructor
        Grafo(Nodo *nodo);
        //función que recorre arbol y agrega datos al archivo txt
        void recorrer(Nodo *p, ofstream &fp);
};
#endif
