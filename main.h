// librerias
#include <string>
#include <unistd.h>
#include <fstream>
#include <iostream>
using namespace std;

// estructura
typedef struct _Nodo {
  struct _Nodo *izq;
  struct _Nodo *der;
  string info;
  int factor_e;
} Nodo;
