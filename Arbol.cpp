// libreria
#include <iostream>
#include <unistd.h>
#include <fstream>
#include <string>
using namespace std;

// clases
#include "main.h"
#include "Grafo.h"
#include "Arbol.h"

#define TRUE 1
#define FALSE 0

// constructor
Arbol::Arbol(){
}

// función que inserta un elemento
void Arbol::InsercionBalanceado(Nodo *&nodo_rev, int *BO, string infor){
	// nodos temporales
	Nodo *nodo = NULL;
	Nodo *nodo1 = NULL;
	Nodo *nodo2 = NULL;
	// variable temporal
	nodo = nodo_rev;

	// cuando el nodo no está vacío
	if(nodo != NULL){
		// si el nodo a agregar es menor que el en revisión retorna un negativo
		if(infor.compare(nodo-> info) <= -1){
			// se ingresa por el lado izquierdo
			InsercionBalanceado(nodo-> izq, BO, infor);
			
			// si el arbol ha crecido
			if(*BO == TRUE){
				// depende de su factor de equlibrio el balanceo que tendrá la inserción
				switch (nodo-> factor_e){
					
					// si el factor_e es 1
					case 1:
						// ahora será 0
						nodo-> factor_e = 0;
						*BO = FALSE;
					break;
					
					// si el factor_e es 0
					case 0:
						// ahora será -1
						nodo->factor_e = -1;
					break;

					// si era -1 se debe reestructurar el arbol
					case -1:
						// se reestrucutra el arbol
						nodo1 = nodo->izq;

						// rotacion
						if(nodo1-> factor_e <= 0){
							nodo-> izq = nodo1-> der;
							nodo1-> der = nodo;
							nodo-> factor_e = 0;
							nodo = nodo1;
						}
						else{
							// rotación de id
							nodo2 = nodo1->der;
							nodo-> izq = nodo2-> der;
							nodo2-> der = nodo;
							nodo1-> der = nodo2-> izq;
							nodo2-> izq = nodo1;

							// cambio dentro de los factor_e según la rotación si tiene elemento en la izquierda
							if(nodo2-> factor_e == -1){
								nodo-> factor_e = 1;
							}
							else{
								nodo->factor_e =0;
							}

							// cambio dentro de los factor_e según la rotación si tiene elemento en la derecho
							if(nodo2-> factor_e == 1){
								nodo1-> factor_e = -1;
							}
							else{
								nodo1-> factor_e = 0;
							}
							nodo = nodo2;
						}
						nodo-> factor_e = 0;
						*BO = FALSE;
					break;
				}
			}
		}
		else{
			// si es mayor al nodo revisado retorna un entero positivo
			if(infor.compare(nodo-> info) >= 1){
				// función inserta por la derecha
				InsercionBalanceado(nodo-> der, BO, infor);

				// si el arbol a crecido
				if(*BO == TRUE){
					// depende de su factor de equlibrio el balanceo que tendrá la inserción
					switch (nodo-> factor_e){

						// si era -1 será 0
						case -1:
							nodo-> factor_e = 0;
							*BO = FALSE;
						break;
						
						// si era 0 será 1
						case 0:
							nodo-> factor_e = 1;
						break;

						// si era 1 entonces se debe reestructurar
						case 1:
							nodo1 = nodo->der;
							if(nodo1-> factor_e >= 0){
								nodo-> der = nodo1-> izq;
								nodo1-> izq = nodo;
								nodo-> factor_e = 0;
								nodo = nodo1;

							}
							else{
								nodo2 = nodo1-> izq;
								nodo-> der = nodo2-> izq;
								nodo2-> izq = nodo;
								nodo1-> izq = nodo2-> der;
								nodo2-> der = nodo1;
								if(nodo2-> factor_e == 1){
									nodo-> factor_e = -1;
								}
								else{
									nodo-> factor_e = 0;
								}
								if(nodo2-> factor_e == -1){
									nodo1-> factor_e = 1;
								}
								else{
									nodo1-> factor_e = 0;
								}
								nodo = nodo2;
							}
							nodo-> factor_e = 0;
							*BO = FALSE;
						break;
					}
				}
			}
			else{
				cout << "\t>> Ya existe en el arbol" << endl;
			}
		}
		// si el nodo no existe
	}
	else{
		// crea nuevo nodo
		nodo = new Nodo;
		nodo-> izq = NULL;
		nodo-> der = NULL;
		nodo-> info = infor;
		nodo-> factor_e = 0;
		*BO = TRUE;
	}
	// se reemplaza con el temporal
	nodo_rev = nodo;
}

// función que busca y verifica que exista también
void Arbol::Busqueda(Nodo *nodo, string infor, bool &existe){
	//si el nodo a revisar existe
	if(nodo != NULL){
		// si el que se desea buscar es menor al que se está revisando
		if(infor.compare(nodo-> info) <= -1){
			// se busca por la izquierda
			Busqueda(nodo-> izq, infor, existe);
		}
		else{
			// si el que se desea buscar es mayor al que se está revisando
			if(infor.compare(nodo-> info) >= 1){
				// se busca por la derecha
				Busqueda(nodo-> der,infor, existe);
			}
			else{
				// si no es mayor ni menor entonces es igual
				cout << "\t>> Existe" << endl;
				existe = true;
			}
		}
	} 
	else{
		cout << "\t>> No existe" << endl;
		existe = false;
	}
}

// función tipo de reestructura a la izquierda cuando se debe eliminar
void Arbol::Restructura1(Nodo **nodocabeza, int *BO){
	Nodo *nodo, *nodo1, *nodo2;
	nodo = *nodocabeza;

	if(*BO == TRUE){
		switch (nodo-> factor_e){
			case -1:
				nodo-> factor_e = 0;
			break;

			case 0:
				nodo-> factor_e = 1;
				*BO = FALSE;
			break;

			case 1:
				// se balancea el arbol dependiendo de su factor de equilibrio
				nodo1 = nodo-> der;

				if(nodo1->factor_e >= 0){
					/* rotacion DD */
					nodo-> der = nodo1->izq;
					nodo1-> izq = nodo;

					switch (nodo1->factor_e){
						
						case 0:
							nodo->factor_e = 1;
							nodo1->factor_e = -1;
							*BO = FALSE;
						break;

						case 1:
							nodo->factor_e = 0;
							nodo1->factor_e = 0;
							*BO = FALSE;
						break;
					}
					nodo = nodo1;
				} 
				else{
					// rotación
					nodo2 = nodo1->izq;
					nodo-> der = nodo2-> izq;
					nodo2-> izq = nodo;
					nodo1-> izq = nodo2-> der;
					nodo2-> der = nodo1;
					if(nodo2->factor_e == 1)
						nodo->factor_e = -1;
					else
						nodo-> factor_e = 0;
						if(nodo2-> factor_e == -1)
							nodo1-> factor_e = 1;
						else
							nodo1-> factor_e = 0;
							nodo = nodo2;
							nodo2-> factor_e = 0;
							*BO = FALSE;
				}
			break;
		}
	}
	*nodocabeza = nodo;
}

// función tipo de reestructura a la derecha cuando se debe eliminar
void Arbol::Restructura2(Nodo **nodocabeza, int *BO){
	Nodo *nodo, *nodo1, *nodo2;
	nodo = *nodocabeza;
	if(*BO == TRUE){
		switch (nodo-> factor_e){
			
			case 1:
				nodo-> factor_e = 0;
			break;

			case 0:
				nodo-> factor_e = -1;
				*BO = FALSE;
			break;

			case -1:
				// se balancea el arbol dependiendo de su factor de equilibrio
				nodo1 = nodo-> izq;
				if(nodo1-> factor_e<=0){
					// rotación
					nodo-> izq = nodo1-> der;
					nodo1-> der = nodo;
					switch (nodo1-> factor_e){
						case 0:
							nodo-> factor_e = -1;
							nodo1-> factor_e = 1;
							*BO = FALSE;
						break;

						case -1:
							nodo-> factor_e = 0;
							nodo1-> factor_e = 0;
							*BO = FALSE;
						break;
					}
					nodo = nodo1;
				}
				else{
					// rotación
					nodo2 = nodo1-> der;
					nodo-> izq = nodo2-> der;
					nodo2-> der = nodo;
					nodo1-> der = nodo2-> izq;
					nodo2-> izq = nodo1;
					if(nodo2-> factor_e == -1)
						nodo-> factor_e = 1;
					else
						nodo-> factor_e = 0;
						if(nodo2-> factor_e == 1)
							nodo1-> factor_e = -1;
						else
							nodo1-> factor_e = 0;
							nodo = nodo2;
							nodo2-> factor_e = 0;
				}
			break;
		}
	}
	*nodocabeza = nodo;
}

// función que borra 
void Arbol::Borra(Nodo **aux1, Nodo **otro1, int *BO){
	// temporales y auxiliares
	Nodo *aux, *otro;
	aux = *aux1;
	otro = *otro1;
	// si existe el de la derecha
	if(aux-> der != NULL){
		// se sigue por ese lado
		// llama a borrar y reestructurar
		Borra(&(aux->der), &otro, BO);
		Restructura2(&aux, BO);
	} 
	// si no existe por la derecha
	else{
		// se elimina el info
		otro-> info = aux-> info;
		aux = aux-> izq;
		*BO = TRUE;
	}
	// se igualan a los temporales
	*aux1 = aux;
	*otro1 = otro;
}

// función que elimina dato del arbol
void Arbol::EliminacionBalanceado(Nodo **nodocabeza, int *BO, string infor){
	// temporales
	Nodo *nodo, *otro;
	nodo = *nodocabeza;
	// si el nodo a revisar existe
	if(nodo != NULL){
		// el que se desea eliminar es menor
		if(infor.compare(nodo-> info) <= -1){
			// se procede a seguir buscando por la izquierda hasta encontrarlo
			EliminacionBalanceado(&(nodo->izq), BO, infor);
			Restructura1(&nodo, BO); //luego se reestrucura
		}
		else{
			// el que se desea eliminar es mayor
			if(infor.compare(nodo-> info) >= 1){
				// se sigue buscando por la derecha hasta encontrarlo
				EliminacionBalanceado(&(nodo-> der), BO, infor);
				Restructura2(&nodo, BO); //luego se reestrucura
			}
			else{
				// se encuentra el nodo a eliminar
				otro = nodo; //temporal
				// no existe nodo por la derecha entonces se reemplaza por el de izquierda
				if(otro-> der == NULL){
					nodo = otro-> izq;
					*BO = TRUE;
					// si existe un nodo a la derecha
				}
				else{
					// pero no por la izquierda, entonces se reemplaza el nodo por el de la derecha
					if(otro-> izq==NULL){
						nodo = otro-> der;
						*BO = TRUE;
						// cuando tenga dos ramas existentes
					}
					else{
						*BO = FALSE;
						// se procede a borrar y reestructurar
						Borra(&(otro->izq), &otro, BO);
						Restructura1(&otro, BO);
						otro = NULL; // se elimina el temporal
					}
				}
			}
		}
	} 
	else{
		cout << "\t No existe en el arbol" << endl;
	}
	*nodocabeza = nodo;
}

// función que crea el grafo
void Arbol::crear_grafo(Nodo *raiz){
	Grafo *g = new Grafo(raiz);
}
