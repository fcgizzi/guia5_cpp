// librerias
#include <string>
#include <unistd.h>
#include <fstream>
#include <iostream>
using namespace std;

// constructores
#ifndef ARBOL_H
#define ARBOL_H

// clase
class Arbol{
  private:
  public:
      //constructor
      Arbol();
      // función que inserta elemento
      void InsercionBalanceado(Nodo *&nodo_rev, int *BO, string infor);
      // función busca un nodo
      void Busqueda(Nodo *nodo, string infor, bool &existe);
      // función reestrucutura hacia la izquierda
      void Restructura1(Nodo **nodocabeza, int *BO);
      // función reestructura hacia la derecha
      void Restructura2(Nodo **nodocabeza, int *BO);
      // función borra un nodo
      void Borra(Nodo **aux1, Nodo **otro1, int *BO);
      // función modifica (busca y elimina un nodo)
      void EliminacionBalanceado(Nodo **nodocabeza, int *BO , string infor);
      // función crea grafo
      void crear_grafo(Nodo *raiz);
};
#endif
